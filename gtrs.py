#!/usr/bin/env python
# -*- coding: utf8 -*-


# Google translater


# Usage examples :
#	python gtrs.py text
#	python gtrs.py -s en -d tr love
#	python gtrs.py -h


import urllib2,re,argparse

	

class Gtrs(object):
	def __init__(self,src_lan,dest_lan):
		self.src_lan=src_lan
		self.dest_lan=dest_lan


	def fetch(self,text):
		self.text=text
		url='http://translate.google.com.tr/translate_a/t?client=t&sl=%s&hl=%s&text=%s'%(self.src_lan,self.dest_lan,text.replace(" ","+"))
		
		req = urllib2.Request(url,headers={'User-Agent': 'Mozilla/5.0'})
		res=urllib2.build_opener().open(req)
		res_list=eval(re.sub("\,+",",",res.read())) #remove more than one commas
		return res_list if res.code==200 and type(res_list[1])==list  else False
		
	def show(self,word_list):
		if word_list:
			print """--> %s <--\n===============\n%s\n"""%(self.text,"\n".join(word_list))
		else:
			print "\nNo result found\n"

		return True		
			



if __name__=="__main__":

	parser = argparse.ArgumentParser(description='Google translater')
	parser.add_argument('text', metavar='Text', type=str, nargs='+')
	parser.add_argument("-s",'--src-lan',help="Source language",default="tr")
	parser.add_argument("-d",'--dest-lan',help="Destination language",default="en")
	
	args=vars(parser.parse_args())
	gtrs=Gtrs(args['src_lan'],args['dest_lan'])
	res=gtrs.fetch(" ".join(args['text']))
	gtrs.show(res[1][0][1] if res else False)
