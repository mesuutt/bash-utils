#!/bin/bash

# PostgreSQL database backuper

# Usage examples :
#	./pgbackuper.sh test_db
#	./pgbackuper.sh test_db /home/mesuutt/backups/
#	./pgbackuper.sh test_db /home/mesuutt/backups/db_backup.sql

USER="postgres"
DBNAME=$1
PGDUMP=/usr/local/pgsql/bin/pg_dump 
DEFAULT_BACKUP_DIR=/usr/local/pgsql/db_dumps/
BACKUP_FILE=${2:-$DEFAULT_BACKUP_DIR}


if [ $# -lt 1 ]; then
  echo "Bad Arguments"
  echo "-------------------------------------------------------"
  echo "USAGE : $0 databasename [ outputfolder | outputfile ]  "
  echo "-------------------------------------------------------"
  exit 1
fi

if [ -d $BACKUP_FILE ]; then
  BACKUP_FILE=$BACKUP_FILE`date +%Y%m%d-%H_%M.sql` # 20120901-13_00.sql
fi

#Compressed backup 
su - $USER -c "$PGDUMP $DBNAME | gzip > $BACKUP_FILE.gz"
echo "$BACKUP_FILE.gz"


exit 0