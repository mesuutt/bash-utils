#!/bin/bash

# Low free memory notifier
#
# Dependencies:
#   notify-send
#
# USAGE:
# Notify user if there is less than 200 MB free space
#   ./mem-notifier.sh &
# Notify user if there is less than 1 GB free space
#   ./mem-notifier.sh 1024 &
#
#
# Default minimum free memory requirement is 200 MB
# But you can change it with give number as second parameter as MB.
#
# You shoud run command at startup with normal user
# For example :
# 	su mesuutt -c '/usr/local/bin/low-mem-notifier.sh 500 &'
#
#
# You shouldn't launch this script with sudo, since it won't have access to dbus.
# notify-send command not work with sudo.


if [[ $EUID -eq 0 ]]; then
	echo "You don't run the script with root. Exiting"
	exit 1
fi

if [ "$1" == "-h" ] || [ "$1" == "--help" ]; then
    echo "Low memory notifier"
    echo "Usage:"
    echo -e "\t$0 [SIZE] & \n"
    echo "Default minimum free memory requirement size 200 MB"
    echo "But you can change it with give number as second parameter as MB."
    exit
fi

# http://stackoverflow.com/a/927949/1027507
CMD=`basename $0`
RUNNING=`ps --no-headers -C ${CMD} | wc -l`
if [[ $RUNNING -gt 2 ]]; then
    echo "Low memory notifier already running. Exiting"
    exit 1;
fi


REQUIRED_MEM_SPACE=${1:-200} # MB
NOTIFY_TIMEOUT=25 # second

LAST_SHOW_TIME=0
while true;
do
    FREE_SPACE=`free -m | grep 'Mem:' | awk '{print $4}'`
    if [ $FREE_SPACE -lt $REQUIRED_MEM_SPACE ]; then
        NOW=$(date +%s)
        DIFF=$(( $NOW-$LAST_SHOW_TIME ))
        if [ $DIFF -gt $NOTIFY_TIMEOUT ]; then
            notify-send "Low Free Memory" "Only $FREE_SPACE MB remained"
            LAST_SHOW_TIME=$NOW
        fi
    fi

    sleep 1
done

exit 0